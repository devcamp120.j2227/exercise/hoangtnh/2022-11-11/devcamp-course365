/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khai báo mảng chứa thông tin khóa học
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(()=>{
    onPageLoading();
   })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm sự kiện được thực thi khi tải trang
function onPageLoading(){
    console.log('%cTrang được tải','color:yellow')
    var vDivPopularElement = $("#popular-card-deck");
    var vDivTrendingElement = $("#trending-card-deck");
    var vPopularCourse = filterPopularCourses (gCoursesDB.courses) ;
    console.log("%c Popular Course: " ,"color:green");
    console.log(vPopularCourse);
    var vTrendingCourses = filterTrendingCourses (gCoursesDB.courses);
    console.log("%cTreding Course: " ,"color:green");
    console.log(vTrendingCourses);

    createCoursesToHtml(vDivPopularElement, vPopularCourse);
    createCoursesToHtml(vDivTrendingElement, vTrendingCourses);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm lọc mảng chứa popular course
function filterPopularCourses(paramCousesArr){
    var vPopularCourse = paramCousesArr.filter(course => course.isPopular === true);
    return vPopularCourse;
}
//hàm lọc mảng chứa trending course
function filterTrendingCourses(paramCousesArr){
    var vTrendingCourses = paramCousesArr.filter(course => course.isTrending === true);
    return vTrendingCourses;
}

//Hàm tạo thông tin khóa học trên website
function createCoursesToHtml(paramElement, paramCousesArr){
    var vDivPopularElement = $("#popular-card-deck");
    var vDivTrendingElement = $("trending-card-deck");

    $(paramElement).html("");
    paramCousesArr.map((course,index)=>{
        var vCourse = `<div class="card">
              <img src= ${course.coverImage} class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">${course.courseName}</h5>
                <p class="card-text"> <i class="fa-regular fa-clock"></i> &nbsp; ${course.duration} ${course.level}</p>
                <div class="row" style="margin-left: 60px">
                  <p style="font-weight: bold">${"$"+ course.discountPrice}</p> &nbsp;
                  <p style="text-decoration:line-through">${"$"+course.price}</p>
                </div>
              </div>
              <div class="card-footer">
                <img  width="20%" src=${course.teacherPhoto} class="rounded-circle">&nbsp;
                <small>${course.teacherName}</small>&nbsp;&nbsp;
                <i class="fa-regular fa-bookmark" style="margin-left: 25px;"></i>
              </div>
            </div>`;
        $(vCourse).appendTo(paramElement);
    });
}