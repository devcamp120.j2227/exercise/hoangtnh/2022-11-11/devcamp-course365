// Import thư viện express JS
const express = require("express");
//import thư viện path
const path = require ("path");

//tạo app 
const app = express();

//khai báo cổng chạy app
const port = 8000;

//call api chạy project course 365
app.get("/",(request, response) =>{
    response.sendFile(path.join(__dirname + "/views/index.html"))
})
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));

//chạy app trên port
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})